<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends Controller
{
    /**
     * @Route("/api/email", name="check_email" ,options = { "expose" = true })
     */
    public function checkEmailAction(Request $request)
    {

        $response = false;

        $email = $request->request->get("email");


        if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)){

            $response = true;

        }

        return new JsonResponse(['email'=> $response]);
    }



}
