<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Adresse;
use AppBundle\Entity\Contact;
use AppBundle\Form\AdresseType;
use AppBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class DefaultController extends Controller
{

    /**
     * @Route("/", name="redirect")
     */
    public function redirectAction()
    {


        return new RedirectResponse($this->generateUrl("homepage"));
    }




    /**
     * @Route("/crm", name="homepage")
     * @Template("default/index.html.twig")
     * @IsGranted("ROLE_ADMIN")
     */
    public function indexAction()
    {

        $user = $this->getUser();
        $contacts = $user->getContacts();

         return[
             "contacts" => $contacts
         ];
    }



    /**
     * @Route("/crm/contact/add", name="add_contact")
     * @Route("/crm/contact/edit/{id}", name="edit_contact")
     * @Template("contact/add.html.twig")
     * @IsGranted("ROLE_ADMIN")
     */
    public function createContactAction(Request $request, Contact $id = null)
    {

        $contact = $id ? $id : new Contact();



        $form = $this->createForm(ContactType::class, $contact);



        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();

            return $this->redirectToRoute('homepage');
        }


        return [
            'form' => $form->createView(),
        ];




    }




    /**
     * @Route("/crm/adresse", name="adresse")
     * @Template("adresse/index.html.twig")
     * @IsGranted("ROLE_ADMIN")
     */
    public function listeAdresseAction()
    {

        $user = $this->getUser();
        $adresses = $this->getDoctrine()->getManager()->getRepository("AppBundle:Adresse")->findAll();

        return[
            "adresses" => $adresses
        ];
    }



    /**
     * @Route("/crm/adresse/add", name="add_adresse")
     * @Route("/crm/adresse/edit/{id}", name="edit_adresse")
     * @Template("adresse/add.html.twig")
     * @IsGranted("ROLE_ADMIN")
     */
    public function createAdresseAction(Request $request, Adresse $id = null)
    {

        $adresse = $id ? $id : new Adresse();



        $form = $this->createForm(AdresseType::class, $adresse);



        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $adresse = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($adresse);
            $entityManager->flush();

            return $this->redirectToRoute('homepage');
        }


        return [
            'form' => $form->createView(),
        ];




    }

}
