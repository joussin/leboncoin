<?php

namespace Tests\AppBundle\Controller;

use Doctrine\Bundle\DoctrineCacheBundle\Tests\TestCase;


class ApiControllerTest extends TestCase
{
    public function testCheckEmail()
    {

        $result =false;

        $email = "joussin@live.com";


        if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)){
            $result = true;
        }

        $this->assertEquals(true, $result);

    }
}
